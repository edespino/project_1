var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing bicicletas', function() {
    beforeEach(function(done) {        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});  
        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'Connection error: '));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({},function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('Se crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, "blanco", "urbana", [21.931293, -102.258488]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("blanco");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(21.931293);
            expect(bici.ubicacion[1]).toEqual(-102.258488);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('Comienza vacía', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('Se crea una bicicleta', (done) => {
            var aBici = new Bicicleta({code: 1, color: "blanco", modelo: "urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, bicis) {
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                })
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('Devolver bicicleta con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici1 = new Bicicleta({code: 1, color: "albiroja", modelo: "urbana"});
                Bicicleta.add(aBici1, function(err, newBici){
                    if(err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "azul", modelo: "urbana2"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, function(err, bici) {
                            expect(bici.code).toBe(1);
                            expect(bici.color).toBe("albiroja");
                            expect(bici.modelo).toBe("urbana");
                            done();
                        });
                    });
                });
            });
        });
    });


});

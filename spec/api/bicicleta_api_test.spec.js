var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
var mongoose = require('mongoose');

var base_url = "http://localhost:3000/api/bicicletas";

describe('Bicicleta API', () => {
    beforeEach(function(done) {        
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true});  
        const db = mongoose.connection;
        db.on('error',console.error.bind(console, 'Connection error: '));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({},function(err, success){
            if (err) console.log(err);
            mongoose.disconnect(err);
            done();
        });
    });

    describe('GET Bicicleta /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error,response,body) {
                var result = JSON.parse.bind(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe('POST Bicicleta /create', () => {
        it('Status 200', (done) => {
            var headers = {'content-type' : 'application/json'};
            var aBici = '{"id": 2,"color": "rojo","modelo": "montaña","latitud" :"21.929217","longitud": "-102.258587"}';
            request.post({
                headers : headers,
                url : base_url + "/create",
                body : aBici
            }, function(error,response,body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe("21.929217");
                expect(bici.ubicacion[1]).toBe("-102.258587");
                done();
            });
        });
    });


    describe('POST Bicicletas /delete', () => {
        it('Status 204', (done) => {
            var a = Bicicleta.createInstance(2, 'azul', 'montaña', [21.929217, -102.258587]);

            Bicicleta.add(a, function (err, newBici) {
                var headers = { 'content-type': 'application/json' };
                var bici = '{ "code": 2 }';

                request.delete({
                    headers: headers,
                    url: base_url+"/delete",
                    body: bici
                }, function (err, resp, body) {
                    expect(resp.statusCode).toBe(204);
                    Bicicleta.allBicis(function (err, newBicis) {
                        expect(newBicis.length).toBe(0);
                        done();
                    });
                });
            });
        });
    });

    describe('POST Bicicletas /update', () => {
        it('Status 200', (done) => {
            var headers = { 'content-type': 'application/json' };
            var bici = '{"id": 2,"color": "azul","modelo": "version2","latitud" :"21.929217","longitud": "-102.258587"}';

            var a = new Bicicleta({
                code: 2,
                color: 'azul',
                modelo: 'montaña',
                ubicacion: [21.929217,-102.258587]
            });

            Bicicleta.add(a, function (err, newBici) {
                request.post({
                    headers: headers,
                    url: base_url + "/update",
                    body: bici
                }, function (err, resp, body) {
                    expect(resp.statusCode).toBe(200);

                    Bicicleta.findByCode(a.code, function (err, targetBici) {
                        console.log(targetBici);
                        expect(targetBici.color).toBe('azul');
                        expect(targetBici.modelo).toBe('montaña');
                        expect(targetBici.ubicacion[0]).toBe(21.929217);
                        expect(targetBici.ubicacion[1]).toBe(-102.258587);
                        done();
                    });
                });
            });
        });
    });
});
